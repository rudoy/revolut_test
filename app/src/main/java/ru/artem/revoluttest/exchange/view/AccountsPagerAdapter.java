package ru.artem.revoluttest.exchange.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.exchange.view.listener.OnFocusAppearedListener;
import ru.artem.revoluttest.exchange.view.listener.OnValidChangedListener;
import ru.artem.revoluttest.exchange.view.listener.OnValueChangedListener;

/**
 * @author Artem Rudoy
 */
public class AccountsPagerAdapter extends android.support.v4.view.PagerAdapter implements RateViewController {

    private final Context context;
    private final SparseArray<CurrencyValueView> currencyValueViewSparseArray;

    private int currentPosition;
    @Nullable
    private OnValueChangedListener onValueChangedListener;
    @Nullable
    private OnFocusAppearedListener onFocusAppearedListener;

    public AccountsPagerAdapter(Context context, List<String> supportedCurrencies,
                                OnValidChangedListener sourceValidChangeListener) {
        this.context = context;
        this.currencyValueViewSparseArray = createRateViews(supportedCurrencies, sourceValidChangeListener);
    }

    public void setOnValueChangedListener(@NonNull OnValueChangedListener onValueChangedListener) {
        this.onValueChangedListener = onValueChangedListener;
    }

    public void setFocusedChangeListener(@NonNull OnFocusAppearedListener onFocusAppearedListener) {
        this.onFocusAppearedListener = onFocusAppearedListener;
    }

    @Override
    public int getCount() {
        return currencyValueViewSparseArray.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        CurrencyValueView currencyValueView = currencyValueViewSparseArray.get(position);
        container.addView(currencyValueView);
        return currencyValueView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public CurrencyValueItem getCurrentRateItem() {
        return getCurrentRateView().getCurrencyValueItem();
    }

    @Override
    public void setValue(Double value) {
        getCurrentRateView().setValue(value);
    }

    @Override
    public void clearValue() {
        setValue(0d);
    }

    @Override
    public void setRates(String rates) {
        getCurrentRateView().setRates(rates);
    }

    @Override
    public void hideRatesView() {
        getCurrentRateView().hideRatesView();
    }

    public void setCurrentPosition(int position) {
        currentPosition = position;
    }

    private SparseArray<CurrencyValueView> createRateViews(List<String> supportedCurrencies, OnValidChangedListener onValidChangedListener) {
        SparseArray<CurrencyValueView> rateItems = new SparseArray<>(supportedCurrencies.size());
        int currentPosition = 0;
        for (String currency : supportedCurrencies) {
            rateItems.put(currentPosition++, createRateView(currency, onValidChangedListener));
        }
        return rateItems;
    }

    private CurrencyValueView createRateView(String currency, OnValidChangedListener onValidChangedListener) {
        CurrencyValueView currencyValueView = new CurrencyValueView(context);
        currencyValueView.setCurrency(currency);
        currencyValueView.setValue(0d);
        currencyValueView.setOnFocusChangeListener((v, hasFocus) -> {
            currencyValueView.hideRatesView();
            if (onFocusAppearedListener != null && hasFocus) {
                onFocusAppearedListener.onFocusAppeared(this);
            }
        });
        currencyValueView.addValueTextWatcher(new NotifyAfterTextChangedTextWatcher());
        if (onValidChangedListener != null) {
            currencyValueView.addValueTextWatcher(new ValidChangeTextWatcher(onValidChangedListener, currencyValueView));
        }
        return currencyValueView;
    }

    private CurrencyValueView getCurrentRateView() {
        return currencyValueViewSparseArray.get(currentPosition);
    }

    public void setPersonalAccounts(List<CurrencyValueItem> personalAccounts) {
        for (int i = 0, nsize = currencyValueViewSparseArray.size(); i < nsize; i++) {
            CurrencyValueView currencyValueView = currencyValueViewSparseArray.valueAt(i);
            currencyValueView.setPersonalBalance(getBalanceByCurrency(currencyValueView.getCurrencyValueItem().getCurrency(), personalAccounts));
        }
    }

    private Double getBalanceByCurrency(String currency, List<CurrencyValueItem> personalAccounts) {
        for (CurrencyValueItem currencyValueItem : personalAccounts) {
            if (currency.equals(currencyValueItem.getCurrency())) {
                return currencyValueItem.getValue();
            }
        }
        return 0d;
    }

    private static class ValidChangeTextWatcher implements TextWatcher {

        private final OnValidChangedListener onValidChangedListener;
        private final CurrencyValueView currencyValueView;
        private boolean lastValidState;

        private ValidChangeTextWatcher(OnValidChangedListener onValidChangedListener, CurrencyValueView currencyValueView) {
            this.onValidChangedListener = onValidChangedListener;
            this.currencyValueView = currencyValueView;
            this.lastValidState = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //empty
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //empty
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (lastValidState != currencyValueView.validate()) {
                lastValidState = !lastValidState;
                onValidChangedListener.onValidChangedListener(lastValidState);
            }
        }
    }

    private class NotifyAfterTextChangedTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //empty
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //empty
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (onValueChangedListener != null) {
                onValueChangedListener.onValueChanged();
            }
        }
    }

}
