package ru.artem.revoluttest.exchange;

import java.util.List;

import javax.inject.Inject;

import ru.artem.revoluttest.RevolutTestApplication;
import ru.artem.revoluttest.RxMvpPresenter;
import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.domain.Rates;
import ru.artem.revoluttest.repository.PersonalAccountRepository;
import ru.artem.revoluttest.timer.Timer;
import rx.SingleSubscriber;
import rx.Subscriber;

/**
 * @author Artem Rudoy
 */
public class ExchangePresenter extends RxMvpPresenter<ExchangeMvpView> {

    private final int PERIOD = 30 * 1000;

    @Inject
    ExchangeRatesLoader exchangeRatesLoader;
    @Inject
    PersonalAccountRepository personalAccountRepository;

    private Timer exchangeActiveCurrencyRatesTimer;
    private Timer exchangeNotActiveCurrencyRatesTimer;

    public void loadRatesForCurrencies(String newActiveCurrency, String newNotActiveCurrency) {
        stopLoading();
        getView().hideRatesOnToolbar();
        getView().hideRatesOnNotActiveView();
        exchangeRatesLoader.updateCurrency(newActiveCurrency, newNotActiveCurrency);
        restartTimer();
    }

    public ExchangePresenter() {
        RevolutTestApplication.getInstance().getSingletonComponent().inject(this);
    }

    public void restartTimer() {
        loadForNotActiveCurrency();
        loadForActiveCurrency();
    }

    public void stopLoading() {
        exchangeActiveCurrencyRatesTimer.stop();
        exchangeNotActiveCurrencyRatesTimer.stop();
        exchangeRatesLoader.stop();
    }

    public void initTimer(String sourceCurrency, String destinationCurrency) {
        exchangeRatesLoader.updateCurrency(sourceCurrency, destinationCurrency);
        exchangeActiveCurrencyRatesTimer = new Timer(PERIOD, this::loadForActiveCurrency);
        exchangeNotActiveCurrencyRatesTimer = new Timer(PERIOD, this::loadForNotActiveCurrency);
    }

    private void loadForNotActiveCurrency() {
        exchangeRatesLoader.loadForNotActiveCurrencyIfNeeded(new UpdatesForNotActiveRateSubscriber(this, exchangeNotActiveCurrencyRatesTimer));
    }

    private void loadForActiveCurrency() {
        exchangeRatesLoader.loadForActiveCurrency(new UpdatesForActiveRateSubscriber(this, exchangeActiveCurrencyRatesTimer));
    }

    public void onViewCreated() {
        subscribeIo(personalAccountRepository.getPersonalAccounts(), new UpdatePersonalAccountSubscriber(this));
    }

    public void apply(CurrencyValueItem sourceCurrencyValueItem) {
        getView().successExchanged();
    }

    private static final class UpdatesForActiveRateSubscriber extends BaseRestartTimerSubscriber {

        private final ExchangePresenter presenter;

        private UpdatesForActiveRateSubscriber(ExchangePresenter presenter,Timer timer) {
            super(timer);
            this.presenter = presenter;
        }

        @Override
        public void onNext(Rates rates) {
            presenter.getView().updateForActive(rates);
            presenter.getView().updateForToolbarRates(rates);
        }
    }

    private static final class UpdatesForNotActiveRateSubscriber extends BaseRestartTimerSubscriber {

        private final ExchangePresenter presenter;

        private UpdatesForNotActiveRateSubscriber(ExchangePresenter presenter,Timer timer) {
            super(timer);
            this.presenter = presenter;
        }

        @Override
        public void onNext(Rates rates) {
            presenter.getView().updateForNotActiveRates(rates);
        }
    }

    private abstract static class BaseRestartTimerSubscriber extends Subscriber<Rates> {

        private final Timer timer;

        private BaseRestartTimerSubscriber(Timer timer) {
            this.timer = timer;
        }

        @Override
        public void onCompleted() {
            timer.start();
        }

        @Override
        public void onError(Throwable e) {
            timer.start();
        }

    }

    private static final class UpdatePersonalAccountSubscriber extends SingleSubscriber<List<CurrencyValueItem>> {

        private final ExchangePresenter presenter;

        private UpdatePersonalAccountSubscriber(ExchangePresenter presenter) {
            this.presenter = presenter;
        }

        @Override
        public void onSuccess(List<CurrencyValueItem> personalAccounts) {
            presenter.getView().setPersonalAccounts(personalAccounts);
        }

        @Override
        public void onError(Throwable error) {
            //empty
        }
    }

}
