package ru.artem.revoluttest.exchange;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.domain.Rates;

/**
 * @author Artem Rudoy
 */
public interface ExchangeMvpView extends MvpView {

    void updateForActive(Rates rates);

    void setPersonalAccounts(List<CurrencyValueItem> personalAccounts);

    void updateForNotActiveRates(Rates rates);

    void hideRatesOnToolbar();

    void hideRatesOnNotActiveView();

    void updateForToolbarRates(Rates rates);

    void successExchanged();
}
