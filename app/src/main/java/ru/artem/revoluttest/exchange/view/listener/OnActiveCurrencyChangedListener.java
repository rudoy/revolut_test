package ru.artem.revoluttest.exchange.view.listener;

/**
 * @author Artem Rudoy
 */
public interface OnActiveCurrencyChangedListener {

    void onActiveCurrencyChanged(String newActiveCurrency, String newNotActiveCurrency);
}
