package ru.artem.revoluttest.exchange.view;

import ru.artem.revoluttest.domain.CurrencyValueItem;

/**
 * @author Artem Rudoy
 */
public interface RateViewController {

    CurrencyValueItem getCurrentRateItem();

    void setValue(Double value);

    void clearValue();

    void setRates(String rates);

    void hideRatesView();
}
