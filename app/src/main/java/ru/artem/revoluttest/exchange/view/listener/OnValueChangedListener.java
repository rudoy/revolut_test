package ru.artem.revoluttest.exchange.view.listener;

/**
 * @author Artem Rudoy
 */
public interface OnValueChangedListener {

    void onValueChanged();
}
