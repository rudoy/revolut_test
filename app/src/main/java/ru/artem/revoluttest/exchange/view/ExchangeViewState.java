package ru.artem.revoluttest.exchange.view;

import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.domain.Rates;

/**
 * @author Artem Rudoy
 */
public class ExchangeViewState {

    private final RateViewController sourceRateViewController;
    private final RateViewController destRateViewController;
    private RateViewController activeRateViewController;
    private Rates lastRates;

    public ExchangeViewState(RateViewController sourceRateViewController, RateViewController destRateViewController) {
        this.sourceRateViewController = sourceRateViewController;
        this.destRateViewController = destRateViewController;
    }

    public void setActiveRateViewController(RateViewController activeRateViewController) {
        if (activeRateViewController != this.activeRateViewController) {
            this.activeRateViewController = activeRateViewController;
        }
    }

    public boolean isActiveController(RateViewController rateViewController) {
        return activeRateViewController == rateViewController;
    }

    public void setRates(Rates rates) {
        lastRates = rates;
        validate(rates);
    }

    public RateViewController getNotActiveRateViewController() {
        return activeRateViewController == sourceRateViewController ? destRateViewController : sourceRateViewController;
    }

    public RateViewController getActiveRateViewController() {
        return activeRateViewController;
    }

    public void recalculateValue() {
        CurrencyValueItem activeCurrencyValueItem = activeRateViewController.getCurrentRateItem();
        CurrencyValueItem notActiveCurrencyValueItem = getNotActiveRateViewController().getCurrentRateItem();
        if (isInSameCurrency(notActiveCurrencyValueItem)) {
            getNotActiveRateViewController().setValue(0d);
        } else {
            convertAndUpdate(activeCurrencyValueItem, notActiveCurrencyValueItem);
        }
    }

    private void convertAndUpdate(CurrencyValueItem activeCurrencyValueItem, CurrencyValueItem notActiveCurrencyValueItem) {
        if (lastRates.getRate(notActiveCurrencyValueItem.getCurrency()) == null) {
            getNotActiveRateViewController().setValue(0d);
        } else {
            Double concreteRate = lastRates.getRate(notActiveCurrencyValueItem.getCurrency());
            Double newValue = concreteRate * activeCurrencyValueItem.getValue();
            getNotActiveRateViewController().setValue(newValue);
        }
    }

    private boolean isInSameCurrency(CurrencyValueItem notActiveCurrencyValueItem) {
        return lastRates.getBase().equals(notActiveCurrencyValueItem.getCurrency());
    }

    private void validate(Rates rates) {
        if (!activeRateViewController.getCurrentRateItem().getCurrency().equals(rates.getBase())) {
            throw new IllegalArgumentException("currencies mismatched");
        }
    }
}
