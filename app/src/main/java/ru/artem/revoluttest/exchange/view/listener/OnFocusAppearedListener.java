package ru.artem.revoluttest.exchange.view.listener;

import ru.artem.revoluttest.exchange.view.RateViewController;

/**
 * @author Artem Rudoy
 */
public interface OnFocusAppearedListener {

    void onFocusAppeared(RateViewController controller);
}
