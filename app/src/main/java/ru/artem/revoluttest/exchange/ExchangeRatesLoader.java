package ru.artem.revoluttest.exchange;

import ru.artem.revoluttest.domain.Rates;
import ru.artem.revoluttest.repository.RatesRepository;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Artem Rudoy
 */
public class ExchangeRatesLoader {

    private final RatesRepository ratesRepository;
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    private String activeCurrency;
    private String notActiveCurrency;

    public void updateCurrency(String activeCurrency, String notActiveCurrency) {
        this.activeCurrency = activeCurrency;
        this.notActiveCurrency = notActiveCurrency;
    }

    public ExchangeRatesLoader(RatesRepository ratesRepository) {
        this.ratesRepository = ratesRepository;
    }

    public void loadForActiveCurrency(Subscriber<Rates> subscriber) {
        loadRates(activeCurrency, subscriber);
    }

    public void stop() {
        compositeSubscription.clear();
    }

    public void loadForNotActiveCurrencyIfNeeded(Subscriber<Rates> subscriber) {
        if (!activeCurrency.equals(notActiveCurrency)) {
            loadRates(notActiveCurrency, subscriber);
        }
    }

    private void loadRates(String currency, Subscriber<Rates> subscriber) {
        compositeSubscription.add(ratesConcatDelayErrorObservable(currency)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread(), true)
                .subscribe(subscriber));
    }

    private Observable<Rates> ratesConcatDelayErrorObservable(String currency) {
        return Observable.concatDelayError(
                Observable.just(ratesRepository.getLocalRates(currency),
                        ratesRepository.getRemoteRates(currency)));
    }
}
