package ru.artem.revoluttest.exchange.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.artem.revoluttest.R;
import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.domain.Rates;
import ru.artem.revoluttest.exchange.view.listener.OnActiveCurrencyChangedListener;
import ru.artem.revoluttest.exchange.view.listener.OnFocusAppearedListener;
import ru.artem.revoluttest.exchange.view.listener.OnValidChangedListener;
import ru.artem.revoluttest.exchange.view.listener.OnValueChangedListener;
import ru.artem.revoluttest.utils.CurrencyUtils;

/**
 * @author Artem Rudoy
 */
public class ExchangeView extends LinearLayout {

    @BindView(R.id.exchange_source_vp)
    ViewPager exchangeSourceViewPager;
    @BindView(R.id.exchange_destinatination_vp)
    ViewPager exchangeDestinationViewPager;

    private AccountsPagerAdapter sourceAdapter;
    private AccountsPagerAdapter destinationAdapter;
    private ExchangeViewState exchangeViewState;

    public ExchangeView(Context context) {
        this(context, null);
    }

    public ExchangeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExchangeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void initView(List<String> currencies,
                         OnActiveCurrencyChangedListener onActiveCurrencyChangedListener,
                         OnValidChangedListener sourceValidChangeListener) {
        sourceAdapter = new AccountsPagerAdapter(getContext(), currencies, sourceValidChangeListener);
        destinationAdapter = new AccountsPagerAdapter(getContext(), currencies, null);
        exchangeViewState = new ExchangeViewState(sourceAdapter, destinationAdapter);

        initAdapter(sourceAdapter, exchangeViewState, onActiveCurrencyChangedListener);
        initAdapter(destinationAdapter, exchangeViewState, onActiveCurrencyChangedListener);

        initViewPager(exchangeSourceViewPager, sourceAdapter, exchangeViewState, onActiveCurrencyChangedListener);
        initViewPager(exchangeDestinationViewPager, destinationAdapter, exchangeViewState, onActiveCurrencyChangedListener);

        exchangeViewState.setActiveRateViewController(sourceAdapter);
    }

    private void initViewPager(ViewPager viewPager,
                               AccountsPagerAdapter adapter,
                               ExchangeViewState exchangeViewState,
                               @Nullable OnActiveCurrencyChangedListener onActiveCurrencyChangedListener) {
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new UpdateCurrencyOrValueOnPageChangeListener(adapter, exchangeViewState, onActiveCurrencyChangedListener));
    }

    private void initAdapter(AccountsPagerAdapter adapter,
                             ExchangeViewState exchangeViewState,
                             @Nullable OnActiveCurrencyChangedListener currencyListener) {
        adapter.setOnValueChangedListener(new RecalсulateValueOnChangedListener(exchangeViewState, adapter));
        adapter.setFocusedChangeListener(new UpdateActiveCurrencyOnFocusAppearedListener(exchangeViewState, currencyListener));
    }

    private void init() {
        setOrientation(VERTICAL);
        inflate(getContext(), R.layout.exchange_view, this);
        ButterKnife.bind(this);
    }

    public void updateRates(Rates rates) {
        exchangeViewState.setRates(rates);
        exchangeViewState.recalculateValue();
    }

    public CurrencyValueItem getSourceRateItem() {
        return sourceAdapter.getCurrentRateItem();
    }

    public CurrencyValueItem getDestinationRateItem() {
        return sourceAdapter.getCurrentRateItem();
    }

    public void setPersonalAccounts(List<CurrencyValueItem> personalAccounts) {
        sourceAdapter.setPersonalAccounts(personalAccounts);
        destinationAdapter.setPersonalAccounts(personalAccounts);
    }

    public void hideRatesOnNotActiveView() {
        exchangeViewState.getNotActiveRateViewController().hideRatesView();
    }

    public void updateForNotActiveRates(Rates rates) {
        CurrencyValueItem activeCurrencyValueItem = exchangeViewState.getActiveRateViewController().getCurrentRateItem();
        CurrencyValueItem notActiveCurrencyValueItem = exchangeViewState.getNotActiveRateViewController().getCurrentRateItem();
        exchangeViewState.getNotActiveRateViewController()
                .setRates(CurrencyUtils.generateRatesString(rates, activeCurrencyValueItem.getCurrency(), notActiveCurrencyValueItem.getCurrency()));
    }

    private static class UpdateActiveCurrencyOnFocusAppearedListener implements OnFocusAppearedListener {

        private final ExchangeViewState exchangeViewState;
        @Nullable
        private final OnActiveCurrencyChangedListener onActiveCurrencyChangedListener;

        private UpdateActiveCurrencyOnFocusAppearedListener(ExchangeViewState exchangeViewState, @Nullable OnActiveCurrencyChangedListener onActiveCurrencyChangedListener) {
            this.exchangeViewState = exchangeViewState;
            this.onActiveCurrencyChangedListener = onActiveCurrencyChangedListener;
        }

        @Override
        public void onFocusAppeared(RateViewController controller) {
            if (!exchangeViewState.isActiveController(controller)) {
                exchangeViewState.setActiveRateViewController(controller);
                if (onActiveCurrencyChangedListener != null) {
                    CurrencyValueItem activeCurrencyValueItem = exchangeViewState.getActiveRateViewController().getCurrentRateItem();
                    CurrencyValueItem notActiveCurrencyValueItem = exchangeViewState.getNotActiveRateViewController().getCurrentRateItem();
                    onActiveCurrencyChangedListener.onActiveCurrencyChanged(activeCurrencyValueItem.getCurrency(), notActiveCurrencyValueItem.getCurrency());
                }
            }
        }
    }

    private static class RecalсulateValueOnChangedListener implements OnValueChangedListener {

        private final ExchangeViewState exchangeViewState;
        private final RateViewController controller;

        public RecalсulateValueOnChangedListener(ExchangeViewState exchangeViewState, RateViewController controller) {
            this.exchangeViewState = exchangeViewState;
            this.controller = controller;
        }

        @Override
        public void onValueChanged() {
            if (exchangeViewState.isActiveController(controller)) {
                exchangeViewState.recalculateValue();
            }
        }
    }
}
