package ru.artem.revoluttest.exchange.view;

import android.view.View;
import android.widget.TextView;

import ru.artem.revoluttest.domain.Rates;
import ru.artem.revoluttest.utils.CurrencyUtils;

/**
 * @author Artem Rudoy
 */
public class ToolbarItemHolder {

    private final TextView textView;
    private String activeCurrency;
    private String notActiveCurrency;

    public ToolbarItemHolder(TextView textView) {
        this.textView = textView;
    }

    public void updateCurrencies(String activeCurrency, String notActiveCurrency) {
        this.activeCurrency = activeCurrency;
        this.notActiveCurrency = notActiveCurrency;
    }

    public void updateRatesAndVisibility(Rates rates) {
        if (activeCurrency.equals(notActiveCurrency)) {
            hide();
        } else if (rates.getBase().equals(activeCurrency)) {
            calculateRateValue(rates);
        } else {
            hide();
        }
    }

    private void calculateRateValue(Rates rates) {
        textView.setVisibility(View.VISIBLE);
        textView.setText(CurrencyUtils.generateRatesString(rates, notActiveCurrency, activeCurrency));
    }

    public void hide() {
        textView.setVisibility(View.INVISIBLE);
    }
}
