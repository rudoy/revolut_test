package ru.artem.revoluttest.exchange.view.listener;

/**
 * @author Artem Rudoy
 */
public interface OnValidChangedListener {

    void onValidChangedListener(boolean isValid);
}
