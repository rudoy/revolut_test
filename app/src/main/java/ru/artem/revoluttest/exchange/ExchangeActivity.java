package ru.artem.revoluttest.exchange;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.artem.revoluttest.R;
import ru.artem.revoluttest.RevolutTestApplication;
import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.domain.Rates;
import ru.artem.revoluttest.exchange.view.ExchangeView;
import ru.artem.revoluttest.exchange.view.ToolbarItemHolder;

/**
 * @author Artem Rudoy
 */
public class ExchangeActivity extends MvpActivity<ExchangeMvpView, ExchangePresenter> implements ExchangeMvpView {

    @BindView(R.id.exchange_view)
    ExchangeView exchangeView;
    @BindView(R.id.toolbarLabel)
    TextView toolbarLabel;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MenuItem applyMenuItem;
    private ToolbarItemHolder toolbarItemHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(null);
        toolbarItemHolder = new ToolbarItemHolder(toolbarLabel);
        toolbarLabel.setOnClickListener(view -> Toast.makeText(this, R.string.success, Toast.LENGTH_SHORT).show());
        exchangeView.initView(getDefaultCurrencies(),
                (newActiveCurrency, newNotActiveCurrency) -> {
                    presenter.loadRatesForCurrencies(newActiveCurrency, newNotActiveCurrency);
                    toolbarItemHolder.updateCurrencies(newActiveCurrency, newNotActiveCurrency);
                }, isValid -> applyMenuItem.setEnabled(isValid));
        String sourceCurrency = exchangeView.getSourceRateItem().getCurrency();
        String destinationCurrency = exchangeView.getDestinationRateItem().getCurrency();
        toolbarItemHolder.updateCurrencies(sourceCurrency, destinationCurrency);
        presenter.initTimer(sourceCurrency, destinationCurrency);
        presenter.onViewCreated();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.exchange_menu, menu);
        applyMenuItem = menu.findItem(R.id.apply_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.apply_menu:
                presenter.apply(exchangeView.getSourceRateItem());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    @Override
    public ExchangePresenter createPresenter() {
        return new ExchangePresenter();
    }

    @Override
    public void updateForActive(Rates rates) {
        exchangeView.updateRates(rates);
    }

    @Override
    public void setPersonalAccounts(List<CurrencyValueItem> personalAccounts) {
        exchangeView.setPersonalAccounts(personalAccounts);
    }

    @Override
    public void updateForNotActiveRates(Rates rates) {
        exchangeView.updateForNotActiveRates(rates);
    }

    @Override
    public void hideRatesOnToolbar() {
        toolbarItemHolder.hide();
    }

    @Override
    public void hideRatesOnNotActiveView() {
        exchangeView.hideRatesOnNotActiveView();
    }

    @Override
    public void updateForToolbarRates(Rates rates) {
        toolbarItemHolder.updateRatesAndVisibility(rates);
    }

    @Override
    public void successExchanged() {
        Toast.makeText(this, R.string.success, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.stopLoading();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.restartTimer();
    }

    private List<String> getDefaultCurrencies() {
        return RevolutTestApplication.getInstance().getSingletonComponent().provideDefaultCurrencies();
    }
}
