package ru.artem.revoluttest.exchange.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.artem.revoluttest.R;
import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.utils.CurrencyUtils;

/**
 * @author Artem Rudoy
 */
public class CurrencyValueView extends RelativeLayout {

    @BindView(R.id.currency_name_tv)
    TextView currencyNameTextView;
    @BindView(R.id.personal_balance_tv)
    TextView personalBalanceTextView;
    @BindView(R.id.count_et)
    EditText countEditText;
    @BindView(R.id.rates_tv)
    TextView ratesTextView;

    private CurrencyValueItem currencyValueItem = new CurrencyValueItem();
    private double personalBalance;

    public CurrencyValueView(Context context) {
        this(context, null, 0);
    }

    public CurrencyValueView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CurrencyValueView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        countEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //empty
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //empty
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    countEditText.setText(Double.toString(0d));
                } else {
                    currencyValueItem.setValue(Double.valueOf(s.toString()));
                }
            }
        });
    }

    public void setValue(Double count) {
        countEditText.setText(Double.toString(count));
    }

    public void setRates(String rates) {
        ratesTextView.setVisibility(VISIBLE);
        ratesTextView.setText(rates);
    }

    public void setCurrency(String currency) {
        currencyValueItem.setCurrency(currency);
        currencyNameTextView.setText(currency);
    }

    public void setPersonalBalance(Double balance) {
        personalBalance = balance;
        personalBalanceTextView.setText(getContext().getString(R.string.you_balace, balance,
                CurrencyUtils.getCurrencySymbol(currencyValueItem.getCurrency())));
    }

    private void init() {
        inflate(getContext(), R.layout.rate_view, this);
        ButterKnife.bind(this);
    }

    public void setOnFocusChangeListener(OnFocusChangeListener onValueFocusListener) {
        countEditText.setOnFocusChangeListener(onValueFocusListener);
    }

    public void addValueTextWatcher(TextWatcher textWatcher) {
        countEditText.addTextChangedListener(textWatcher);
    }

    public CurrencyValueItem getCurrencyValueItem() {
        return currencyValueItem;
    }

    public void hideRatesView() {
        ratesTextView.setVisibility(GONE);
    }

    public boolean validate() {
        boolean isValid = personalBalance > currencyValueItem.getValue();
        personalBalanceTextView.setTextColor(getColor(isValid));
        return isValid;
    }

    private int getColor(boolean isValid) {
        return isValid ? ContextCompat.getColor(getContext(), android.R.color.white) :
                ContextCompat.getColor(getContext(), android.R.color.holo_red_light);
    }
}
