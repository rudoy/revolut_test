package ru.artem.revoluttest.exchange.view;

import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import ru.artem.revoluttest.domain.CurrencyValueItem;
import ru.artem.revoluttest.exchange.view.listener.OnActiveCurrencyChangedListener;

/**
 * @author Artem Rudoy
 */
public class UpdateCurrencyOrValueOnPageChangeListener implements ViewPager.OnPageChangeListener {

    private final AccountsPagerAdapter currentAdapter;
    private final ExchangeViewState exchangeViewState;
    @Nullable
    private final OnActiveCurrencyChangedListener onActiveCurrencyChangedListener;

    public UpdateCurrencyOrValueOnPageChangeListener(AccountsPagerAdapter currentAdapter,
                                                     ExchangeViewState exchangeViewState,
                                                     @Nullable OnActiveCurrencyChangedListener onActiveCurrencyChangedListener) {
        this.onActiveCurrencyChangedListener = onActiveCurrencyChangedListener;
        this.currentAdapter = currentAdapter;
        this.exchangeViewState = exchangeViewState;
    }

    @Override
    public void onPageSelected(int position) {
        currentAdapter.setCurrentPosition(position);
        updateActiveCurrencyOrValue();
        if (onActiveCurrencyChangedListener != null) {
            CurrencyValueItem activeCurrencyValueItem = exchangeViewState.getActiveRateViewController().getCurrentRateItem();
            CurrencyValueItem notActiveCurrencyValueItem = exchangeViewState.getNotActiveRateViewController().getCurrentRateItem();
            onActiveCurrencyChangedListener.onActiveCurrencyChanged(activeCurrencyValueItem.getCurrency(), notActiveCurrencyValueItem.getCurrency());
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // empty
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // empty
    }

    private void updateActiveCurrencyOrValue() {
        if (exchangeViewState.isActiveController(currentAdapter)) {
            currentAdapter.clearValue();
        } else {
            exchangeViewState.recalculateValue();
        }
    }
}
