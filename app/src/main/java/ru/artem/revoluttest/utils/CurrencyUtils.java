package ru.artem.revoluttest.utils;

import ru.artem.revoluttest.domain.Currency;
import ru.artem.revoluttest.domain.Rates;

/**
 * @author Artem Rudoy
 */
public class CurrencyUtils {

    private final static String EMPTY_STRING = "";
    private CurrencyUtils() {
        //no instance
    }

    public static String getCurrencySymbol(String currency) {
        switch (currency) {
            case Currency.EURO:
                return "\u20AC";
            case Currency.USD:
                return "\u0024";
            case Currency.GBP:
                return "\u00A3";
            default:
                throw new IllegalArgumentException("cannot found symbol for " + currency);
        }
    }

    public static String generateRatesString(Rates rates, String activeCurrency, String notActiveCurrency) {
        if (rates.getRate(activeCurrency) != null) {
            return "1" + CurrencyUtils.getCurrencySymbol(notActiveCurrency)
                    + " = " + rates.getRate(activeCurrency).toString() + " " + CurrencyUtils.getCurrencySymbol(activeCurrency);
        } else {
            return EMPTY_STRING;
        }
    }
}
