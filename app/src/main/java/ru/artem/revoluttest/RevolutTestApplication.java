package ru.artem.revoluttest;

import android.app.Application;

import ru.artem.revoluttest.dagger.DaggerSingletonComponent;
import ru.artem.revoluttest.dagger.MainModule;
import ru.artem.revoluttest.dagger.SingletonComponent;

/**
 * @author Artem Rudoy
 */
public class RevolutTestApplication extends Application {
    private static RevolutTestApplication instance;

    public static RevolutTestApplication getInstance() {
        return instance;
    }

    private SingletonComponent singletonComponent;

    public SingletonComponent getSingletonComponent() {
        return singletonComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        MainModule mainModule = new MainModule(this);
        singletonComponent = DaggerSingletonComponent.builder()
                .mainModule(mainModule)
                .build();
    }
}
