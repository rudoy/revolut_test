package ru.artem.revoluttest.network;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.artem.revoluttest.domain.Rates;
import rx.Observable;

/**
 * @author Artem Rudoy
 */
public interface FixerApi {
    @GET("latest")
    Observable<Rates> rates(@Query("base") String currency);
}
