package ru.artem.revoluttest.dagger.qualifier;

import javax.inject.Qualifier;

/**
 * @author Artem Rudoy
 */
@Qualifier
public @interface IoSchedulerStrategy {

}
