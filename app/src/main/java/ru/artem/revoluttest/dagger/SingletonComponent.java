package ru.artem.revoluttest.dagger;

import java.util.List;

import javax.inject.Singleton;

import dagger.Component;
import ru.artem.revoluttest.exchange.ExchangePresenter;

/**
 * @author Artem Rudoy
 */
@Singleton
@Component(modules = {NetworkModule.class, MainModule.class, ExchangeModule.class})
public interface SingletonComponent {

    void inject(ExchangePresenter exchangePresenter);

    List<String> provideDefaultCurrencies();
}
