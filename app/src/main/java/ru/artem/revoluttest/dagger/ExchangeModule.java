package ru.artem.revoluttest.dagger;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.artem.revoluttest.datasource.RatesInMemoryLocalDatasource;
import ru.artem.revoluttest.datasource.RatesRemoteDatasource;
import ru.artem.revoluttest.exchange.ExchangeRatesLoader;
import ru.artem.revoluttest.network.FixerApi;
import ru.artem.revoluttest.repository.MockedPersonalAccountRepository;
import ru.artem.revoluttest.repository.PersonalAccountRepository;
import ru.artem.revoluttest.repository.RatesRepository;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Artem Rudoy
 */
@Module
public class ExchangeModule {

    @Provides
    @Singleton
    static RatesRemoteDatasource provideRatesRemoteDatasource(FixerApi fixerApi) {
        return new RatesRemoteDatasource(fixerApi);
    }

    @Provides
    @Singleton
    static RatesInMemoryLocalDatasource provideRatesInMemoryLocalDatasource(List<String> defaultCurrencies) {
        return new RatesInMemoryLocalDatasource(defaultCurrencies);
    }

    @Provides
    @Singleton
    static RatesRepository provideGetRatesRepository(RatesInMemoryLocalDatasource inMemoryLocalDatasource, RatesRemoteDatasource remoteDatasource) {
        return new RatesRepository(remoteDatasource, inMemoryLocalDatasource);
    }

    @Provides
    @Singleton
    static PersonalAccountRepository providePersonalAccountRepository(List<String> defaultCurrencies) {
        return new MockedPersonalAccountRepository(defaultCurrencies);
    }

    @Provides
    @Singleton
    static ExchangeRatesLoader provideExchangeRatesLoader(RatesRepository ratesRepository) {
        return new ExchangeRatesLoader(ratesRepository);
    }
}
