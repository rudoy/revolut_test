package ru.artem.revoluttest.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.artem.revoluttest.domain.Currency;

/**
 * @author Artem Rudoy
 */
@Module
public class MainModule {

    Context context;

    public MainModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    List<String> provideDefaultCurrencies() {
        List<String> supportedRates = new ArrayList<>();
        supportedRates.add(Currency.USD);
        supportedRates.add(Currency.GBP);
        supportedRates.add(Currency.EURO);
        return supportedRates;
    }

}
