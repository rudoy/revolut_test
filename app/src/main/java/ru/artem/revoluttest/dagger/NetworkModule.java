package ru.artem.revoluttest.dagger;

import android.content.Context;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.artem.revoluttest.BuildConfig;
import ru.artem.revoluttest.R;
import ru.artem.revoluttest.dagger.qualifier.IoSchedulerStrategy;
import ru.artem.revoluttest.network.FixerApi;
import rx.Observable;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Artem Rudoy
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    static Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    static Retrofit.Builder provideRetrofitBuilder(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @Singleton
    static OkHttpClient provideOkHttpClient(Context context) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return new OkHttpClient.Builder()
                .readTimeout(context.getResources().getInteger(R.integer.read_timeout_sec), TimeUnit.SECONDS)
                .writeTimeout(context.getResources().getInteger(R.integer.write_timeout_sec), TimeUnit.SECONDS)
                .connectTimeout(context.getResources().getInteger(R.integer.connect_timeout_sec), TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    static FixerApi provideFixerApi(Retrofit.Builder builder, Context context) {
        builder.baseUrl(context.getString(R.string.fixer_api_url));
        return builder.build().create(FixerApi.class);
    }

    @Singleton
    @IoSchedulerStrategy
    @Provides
    static Observable.Transformer<Object, Object> provideObservableIoSchedulerStrategy() {
        return observable -> observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread(), true);
    }

    @Singleton
    @IoSchedulerStrategy
    @Provides
    static Single.Transformer<Object, Object> provideSingleIoSchedulerStrategy() {
        return single -> single.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}
