package ru.artem.revoluttest.domain;

/**
 * @author Artem Rudoy
 */
public final class Currency {

    private Currency() {
        //no instance
    }

    public static final String USD = "USD";
    public static final String GBP = "GBP";
    public static final String EURO = "EUR";
}
