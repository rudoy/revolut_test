package ru.artem.revoluttest.domain;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Artem Rudoy
 */
public class Rates {
    @SerializedName("base")
    String base;
    @SerializedName("rates")
    Map<String, Double> rates;

    public Rates(String base) {
        this.base = base;
        rates = new HashMap<>();
    }

    public String getBase() {
        return base;
    }

    public Double getRate(String currency) {
        return rates.get(currency);
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }
}
