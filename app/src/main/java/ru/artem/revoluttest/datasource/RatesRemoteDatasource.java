package ru.artem.revoluttest.datasource;

import ru.artem.revoluttest.domain.Rates;
import ru.artem.revoluttest.network.FixerApi;
import rx.Observable;

/**
 * @author Artem Rudoy
 */
public class RatesRemoteDatasource {

    private final FixerApi fixerApi;

    public RatesRemoteDatasource(FixerApi fixerApi) {
        this.fixerApi = fixerApi;
    }

    public Observable<Rates> getRate(String currency) {
        return fixerApi.rates(currency);
    }
}
