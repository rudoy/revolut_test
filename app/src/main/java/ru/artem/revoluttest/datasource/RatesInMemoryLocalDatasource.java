package ru.artem.revoluttest.datasource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.artem.revoluttest.domain.Rates;
import rx.Observable;

/**
 * @author Artem Rudoy
 */
public class RatesInMemoryLocalDatasource {
    private List<Rates> rates ;

    public RatesInMemoryLocalDatasource(List<String> supportedRates) {
        this.rates = initDefaultRates(supportedRates);
    }

    public void update(Rates rate) {
        for (Rates currentRate : rates) {
            if (currentRate.getBase().equals(rate.getBase())) {
                currentRate.setRates(rate.getRates());
                return;
            }
        }
        rates.add(rate);
    }

    public Observable<Rates> getRate(String currency) {
        return Observable.from(rates).filter(rate -> rate.getBase().equals(currency)).first();
    }

    private List<Rates> initDefaultRates(List<String> defaultCurrencies) {
        List<Rates> rates = new ArrayList<>();
        for (String currency : defaultCurrencies) {
            rates.add(new Rates(currency));
        }
        return Collections.synchronizedList(rates);
    }
}
