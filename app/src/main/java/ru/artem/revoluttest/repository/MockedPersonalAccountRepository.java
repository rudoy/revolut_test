package ru.artem.revoluttest.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.artem.revoluttest.domain.CurrencyValueItem;
import rx.Single;

/**
 * @author Artem Rudoy
 */
public class MockedPersonalAccountRepository implements PersonalAccountRepository {

    private final List<CurrencyValueItem> currencyValueItemList;

    public MockedPersonalAccountRepository(List<String> defaultCurrencies) {
        currencyValueItemList = new ArrayList<>(defaultCurrencies.size());
        Random random = new Random();
        for (String currencies : defaultCurrencies) {
            currencyValueItemList.add(new CurrencyValueItem(currencies, random.nextDouble() * 100));
        }
    }

    @Override
    public Single<List<CurrencyValueItem>> getPersonalAccounts() {
        return Single.just(currencyValueItemList);
    }
}
