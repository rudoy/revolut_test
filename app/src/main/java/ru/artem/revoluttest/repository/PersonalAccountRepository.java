package ru.artem.revoluttest.repository;

import java.util.List;

import ru.artem.revoluttest.domain.CurrencyValueItem;
import rx.Single;

/**
 * @author Artem Rudoy
 */
public interface PersonalAccountRepository {

    Single<List<CurrencyValueItem>> getPersonalAccounts();
}
