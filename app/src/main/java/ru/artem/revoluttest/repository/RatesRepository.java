package ru.artem.revoluttest.repository;

import ru.artem.revoluttest.datasource.RatesInMemoryLocalDatasource;
import ru.artem.revoluttest.datasource.RatesRemoteDatasource;
import ru.artem.revoluttest.domain.Rates;
import rx.Observable;

/**
 * @author Artem Rudoy
 */
public class RatesRepository {

    private final RatesRemoteDatasource remoteDatasource;
    private final RatesInMemoryLocalDatasource localDatasource;

    public RatesRepository(RatesRemoteDatasource remoteDatasource, RatesInMemoryLocalDatasource localDatasource) {
        this.remoteDatasource = remoteDatasource;
        this.localDatasource = localDatasource;
    }

    public Observable<Rates> getRemoteRates(String currency) {
        return remoteDatasource.getRate(currency)
                .doOnNext(localDatasource::update);
    }

    public Observable<Rates> getLocalRates(String currency) {
        return localDatasource.getRate(currency);
    }

}
