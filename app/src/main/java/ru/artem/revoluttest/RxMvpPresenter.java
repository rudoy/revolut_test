package ru.artem.revoluttest;

import android.support.annotation.CallSuper;
import android.support.annotation.VisibleForTesting;

import com.hannesdorfmann.mosby3.mvp.MvpNullObjectBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import javax.inject.Inject;

import ru.artem.revoluttest.dagger.qualifier.IoSchedulerStrategy;
import rx.Observable;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Artem Rudoy
 */
public abstract class RxMvpPresenter<V extends MvpView> extends MvpNullObjectBasePresenter<V> {

    @Inject
    @IoSchedulerStrategy
    Observable.Transformer<Object, Object> observableSchedulerStrategy;
    @Inject
    @IoSchedulerStrategy
    Single.Transformer<Object, Object> singleSchedulerStrategy;

    private final CompositeSubscription compositeSubscription = new CompositeSubscription();

    @Override
    @CallSuper
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            unSubscribe();
        }
    }

    protected <T> void subscribeIo(Observable<T> observable, Subscriber<T> subscriber) {
        addToCompositeSubscription(observable.compose((Observable.Transformer<T, T>) observableSchedulerStrategy).subscribe(subscriber));
    }

    protected <T> void subscribeIo(Single<T> single, SingleSubscriber<T> subscriber) {
        addToCompositeSubscription(single.compose((Single.Transformer<T, T>) singleSchedulerStrategy).subscribe(subscriber));
    }

    protected void addToCompositeSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    private void unSubscribe() {
        compositeSubscription.clear();
    }
}
