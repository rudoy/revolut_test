package ru.artem.revoluttest.timer;

import android.os.Handler;
import android.os.Looper;

/**
 * @author Artem Rudoy
 */
public class Timer {

    private final Runnable action;
    private final long timeout;
    private final Handler handler;

    public Timer(long timeout, Runnable action) {
        this.timeout = timeout;
        this.action = action;
        handler = new Handler(Looper.getMainLooper());
    }

    public void start() {
        handler.postDelayed(action, timeout);
    }

    public void stop() {
        handler.removeCallbacks(action);
    }
}
